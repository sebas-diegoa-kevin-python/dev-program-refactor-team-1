import pytest
from src.teams_files_reader import get_data_as_dict_v2, create_dir, write_summary, write_row_data
from src.participant_builder import normalize_in_meeting, verify_not_None_values
from src.participant import ParticipantList


def test_data_to_dict_v2_not_none():
    answer = get_data_as_dict_v2(None)
    assert answer is not None, 'Answer should not be None'
    assert answer == [], 'Answer should be like []'
        

def test_data_to_dict_v2_empty_list():
    answer = get_data_as_dict_v2([])
    assert answer is not None, 'Answer should not be None'
    assert answer == [], 'Answer should be like []'


def test_data_to_dict_v2_correct_input():
    input = [                
                (2, ['Name', 'Join time', 'Leave time', 'Duration', 'Email', 'Role']), 
                (3, ['Diego Terrazas Sanchez', '10/10/22, 5:37:54 PM', '10/10/22, 5:38:21 PM', '26s', 
                 'Diego.Terrazas@fundacion-jala.org', 'Presenter'])
            ]
    
    expected = [
            {
                'Name': 'Diego Terrazas Sanchez',
                'Join time': '10/10/22, 5:37:54 PM',
                'Leave time': '10/10/22, 5:38:21 PM',
                'Duration': '26s',
                'Email': 'Diego.Terrazas@fundacion-jala.org',
                'Role': 'Presenter'
            }
        ]
    answer = get_data_as_dict_v2(input)
    assert answer is not None, 'Answer should not be None'
    assert answer == expected, 'Answer should be like expected'


def test_normalize_in_meeting():
    data = {
            'Full Name': 'Diego Terrazas Sanchez', 
            'Join Time': '10/10/22, 5:37:54 PM', 
            'Leave Time': '10/10/22, 5:38:21 PM', 
            'Duration': '26s', 
            'Email': 'Diego.Terrazas@fundacion-jala.org', 
            'Role': 'Presenter'
        }
    
    expected = {
            'Name': 'Diego Terrazas Sanchez', 
            'Join time': '10/10/22, 5:37:54 PM', 
            'Leave time': '10/10/22, 5:38:21 PM', 
            'Duration': '26s', 
            'Email': 'Diego.Terrazas@fundacion-jala.org', 
            'Role': 'Presenter'
        }
    
    answer = normalize_in_meeting(data)
    assert answer is not None, 'Answer should not be None'
    assert answer == expected, 'Answer should be like expected'


def test_verify_not_none_values_incorrect_data():
    data = {
            'Name': 'Diego Terrazas Sanchez', 
            'Join time': '10/10/22, 5:37:54 PM', 
            'Leave time': '10/10/22, 5:38:21 PM', 
            'Duration': '26s', 
            'Email': 'Diego.Terrazas@fundacion-jala.org', 
            'Role': None
        }
    
    match="Some value is giving None - error during normalize in meetings activities."
    with pytest.raises(expected_exception=Exception, match=match):
        verify_not_None_values(data)


def test_verify_not_none_values_correct_data():
    data = {
            'Name': 'Diego Terrazas Sanchez', 
            'Join time': '10/10/22, 5:37:54 PM', 
            'Leave time': '10/10/22, 5:38:21 PM', 
            'Duration': '26s', 
            'Email': 'Diego.Terrazas@fundacion-jala.org', 
            'Role': 'Presenter'
        }    
    try:
        verify_not_None_values(data)
    except Exception as e:
        assert True is False, 'The Function verify_not_None_values should not raise an Exception error.'            


def test_create_dir_fake_path():
    fake_folder = "fake_folder"
    fake_dir = "fake_dir"
    folder = create_dir(fake_dir, fake_folder)
    assert folder == "fake_folder/fake_dir"


def test_create_dir_none_path():
    fake_folder = None
    fake_dir = None
    folder = create_dir(fake_dir, fake_folder)
    assert folder == "Folder cannot be None"


def test_create_dir_empty_path():
    fake_folder = ""
    fake_dir = ""
    folder = create_dir(fake_dir, fake_folder)
    assert folder == "Folder cannot be Empty"


def test_write_summary_accept_only_summary_obj():
    with pytest.raises(expected_exception = TypeError):
        write_summary('fake','fake')


def test_write_wor_data_empty_file():
     with pytest.raises(expected_exception = FileNotFoundError):
        write_row_data('','fake')


def test_write_wor_data_ok():
    temp = write_row_data('temp.csv',['test', 'ok'])
    assert temp == {'statusCode': 'ok', 'body': None}



def test_in_meeting_activities ():
    in_meeting_activities = [
        {'Name': 'Andre Arroyo Hinojosa', 'Join time': '11/14/22, 4:54:29 PM', 'Leave time': '11/14/22, 6:42:38 PM', 'Duration': '1h 48m 8s', 'Email': 'Andre.Arroyo@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Miguel Pozo Morales', 'Join time': '11/14/22, 4:54:45 PM', 'Leave time': '11/14/22, 6:42:37 PM', 'Duration': '1h 47m 52s', 'Email': 'Miguel.Pozo@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Kevin Butvilofsky', 'Join time': '11/14/22, 4:56:16 PM', 'Leave time': '11/14/22, 6:42:36 PM', 'Duration': '1h 46m 19s', 'Email': 'Kevin.Butvilofsky@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Miguel Paricahua Lopez', 'Join time': '11/14/22, 4:56:16 PM', 'Leave time': '11/14/22, 6:42:38 PM', 'Duration': '1h 46m 22s', 'Email': 'Miguel.Paricahua@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Rodrigo Morales Rivas', 'Join time': '11/14/22, 4:56:33 PM', 'Leave time': '11/14/22, 6:42:39 PM', 'Duration': '1h 46m 5s', 'Email': 'Rodrigo.Morales@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Renan Boaventura', 'Join time': '11/14/22, 4:56:55 PM', 'Leave time': '11/14/22, 6:42:51 PM', 'Duration': '1h 45m 55s', 'Email': 'Renan.Boaventura@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Dayana Hernandez Aquize', 'Join time': '11/14/22, 4:57:38 PM', 'Leave time': '11/14/22, 6:42:42 PM', 'Duration': '1h 45m 3s', 'Email': 'Dayana.Hernandez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Diego Quisbert Gutierrez', 'Join time': '11/14/22, 4:58:45 PM', 'Leave time': '11/14/22, 6:42:36 PM', 'Duration': '1h 43m 51s', 'Email': 'Diego.Quisbert@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Miguel Gemio Quispe', 'Join time': '11/14/22, 4:59:15 PM', 'Leave time': '11/14/22, 6:36:50 PM', 'Duration': '1h 37m 34s', 'Email': 'Miguel.Gemio@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Maria Marquina Guevara', 'Join time': '11/14/22, 4:59:24 PM', 'Leave time': '11/14/22, 6:42:35 PM', 'Duration': '1h 43m 11s', 'Email': 'Maria.Marquina@jala.university', 'Role': 'Organizer'}, 
        {'Name': 'Israel Zurita Sanchez', 'Join time': '11/14/22, 4:59:40 PM', 'Leave time': '11/14/22, 6:37:04 PM', 'Duration': '1h 37m 24s', 'Email': 'Israel.Zurita@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Israel Zurita Sanchez', 'Join time': '11/14/22, 6:41:00 PM', 'Leave time': '11/14/22, 6:42:27 PM', 'Duration': '1m 26s', 'Email': 'Israel.Zurita@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Alejandra Maldonado', 'Join time': '11/14/22, 5:00:15 PM', 'Leave time': '11/14/22, 6:42:45 PM', 'Duration': '1h 42m 29s', 'Email': 'Alejandra.Maldonado@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Diego Alarcon Inturias', 'Join time': '11/14/22, 5:00:28 PM', 'Leave time': '11/14/22, 5:32:14 PM', 'Duration': '31m 45s', 'Email': 'Diego.Alarcon@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Diego Alarcon Inturias', 'Join time': '11/14/22, 5:45:38 PM', 'Leave time': '11/14/22, 6:42:49 PM', 'Duration': '57m 11s', 'Email': 'Diego.Alarcon@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Nelson Delgado Colque', 'Join time': '11/14/22, 5:00:29 PM', 'Leave time': '11/14/22, 6:01:43 PM', 'Duration': '1h 1m 14s', 'Email': 'Nelson.Delgado@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Nelson Delgado Colque', 'Join time': '11/14/22, 6:12:42 PM', 'Leave time': '11/14/22, 6:37:32 PM', 'Duration': '24m 49s', 'Email': 'Nelson.Delgado@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Ivan Gonzalez de la Torre', 'Join time': '11/14/22, 5:00:45 PM', 'Leave time': '11/14/22, 6:42:47 PM', 'Duration': '1h 42m 1s', 'Email': 'Ivan.Gonzalez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Sebastian Arbelaez Meneses', 'Join time': '11/14/22, 5:01:04 PM', 'Leave time': '11/14/22, 6:30:42 PM', 'Duration': '1h 29m 37s', 'Email': 'Sebastian.Arbelaez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Brayan Vasquez Rojas', 'Join time': '11/14/22, 5:02:04 PM', 'Leave time': '11/14/22, 6:42:40 PM', 'Duration': '1h 40m 36s', 'Email': 'Brayan.Vasquez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Alex Perez Castillo', 'Join time': '11/14/22, 5:02:08 PM', 'Leave time': '11/14/22, 6:42:41 PM', 'Duration': '1h 40m 32s', 'Email': 'Alex.Perez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Vanessa Tejerina Flores', 'Join time': '11/14/22, 5:18:39 PM', 'Leave time': '11/14/22, 6:42:38 PM', 'Duration': '1h 23m 58s', 'Email': 'Vanessa.Tejerina@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Jose Mendoza Antonio', 'Join time': '11/14/22, 6:34:47 PM', 'Leave time': '11/14/22, 6:35:37 PM', 'Duration': '49s', 'Email': 'Jose.Mendoza@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Jose Mendoza Antonio', 'Join time': '11/14/22, 6:42:11 PM', 'Leave time': '11/14/22, 6:42:45 PM', 'Duration': '33s', 'Email': 'Jose.Mendoza@jala.university', 'Role': 'Presenter'}]

    expected_participants = [
        {'Name': 'Andre Arroyo Hinojosa', 'First join': '11/14/22, 4:54:29 PM', 'Last leave': '11/14/22, 6:42:38 PM', 'In-meeting duration': '1h 48m 8s', 'Email': 'Andre.Arroyo@jala.university', 'Participant ID (UPN)': 'Andre.Arroyo@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Miguel Pozo Morales', 'First join': '11/14/22, 4:54:45 PM', 'Last leave': '11/14/22, 6:42:37 PM', 'In-meeting duration': '1h 47m 52s', 'Email': 'Miguel.Pozo@jala.university', 'Participant ID (UPN)': 'Miguel.Pozo@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Kevin Butvilofsky', 'First join': '11/14/22, 4:56:16 PM', 'Last leave': '11/14/22, 6:42:36 PM', 'In-meeting duration': '1h 46m 19s', 'Email': 'Kevin.Butvilofsky@jala.university', 'Participant ID (UPN)': 'Kevin.Butvilofsky@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Miguel Paricahua Lopez', 'First join': '11/14/22, 4:56:16 PM', 'Last leave': '11/14/22, 6:42:38 PM', 'In-meeting duration': '1h 46m 22s', 'Email': 'Miguel.Paricahua@jala.university', 'Participant ID (UPN)': 'Miguel.Paricahua@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Rodrigo Morales Rivas', 'First join': '11/14/22, 4:56:33 PM', 'Last leave': '11/14/22, 6:42:39 PM', 'In-meeting duration': '1h 46m 5s', 'Email': 'Rodrigo.Morales@jala.university', 'Participant ID (UPN)': 'Rodrigo.Morales@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Renan Boaventura', 'First join': '11/14/22, 4:56:55 PM', 'Last leave': '11/14/22, 6:42:51 PM', 'In-meeting duration': '1h 45m 55s', 'Email': 'Renan.Boaventura@jala.university', 'Participant ID (UPN)': 'Renan.Boaventura@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Dayana Hernandez Aquize', 'First join': '11/14/22, 4:57:38 PM', 'Last leave': '11/14/22, 6:42:42 PM', 'In-meeting duration': '1h 45m 3s', 'Email': 'Dayana.Hernandez@jala.university', 'Participant ID (UPN)': 'Dayana.Hernandez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Diego Quisbert Gutierrez', 'First join': '11/14/22, 4:58:45 PM', 'Last leave': '11/14/22, 6:42:36 PM', 'In-meeting duration': '1h 43m 51s', 'Email': 'Diego.Quisbert@jala.university', 'Participant ID (UPN)': 'Diego.Quisbert@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Miguel Gemio Quispe', 'First join': '11/14/22, 4:59:15 PM', 'Last leave': '11/14/22, 6:36:50 PM', 'In-meeting duration': '1h 37m 34s', 'Email': 'Miguel.Gemio@jala.university', 'Participant ID (UPN)': 'Miguel.Gemio@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Maria Marquina Guevara', 'First join': '11/14/22, 4:59:24 PM', 'Last leave': '11/14/22, 6:42:35 PM', 'In-meeting duration': '1h 43m 11s', 'Email': 'Maria.Marquina@jala.university', 'Participant ID (UPN)': 'Maria.Marquina@jala.university', 'Role': 'Organizer'}, 
        {'Name': 'Israel Zurita Sanchez', 'First join': '11/14/22, 4:59:40 PM', 'Last leave': '11/14/22, 6:42:27 PM', 'In-meeting duration': '1h 38m 50s', 'Email': 'Israel.Zurita@jala.university', 'Participant ID (UPN)': 'Israel.Zurita@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Alejandra Maldonado', 'First join': '11/14/22, 5:00:15 PM', 'Last leave': '11/14/22, 6:42:45 PM', 'In-meeting duration': '1h 42m 29s', 'Email': 'Alejandra.Maldonado@jala.university', 'Participant ID (UPN)': 'Alejandra.Maldonado@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Diego Alarcon Inturias', 'First join': '11/14/22, 5:00:28 PM', 'Last leave': '11/14/22, 6:42:49 PM', 'In-meeting duration': '1h 28m 56s', 'Email': 'Diego.Alarcon@jala.university', 'Participant ID (UPN)': 'Diego.Alarcon@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Nelson Delgado Colque', 'First join': '11/14/22, 5:00:29 PM', 'Last leave': '11/14/22, 6:37:32 PM', 'In-meeting duration': '1h 26m 3s', 'Email': 'Nelson.Delgado@jala.university', 'Participant ID (UPN)': 'Nelson.Delgado@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Ivan Gonzalez de la Torre', 'First join': '11/14/22, 5:00:45 PM', 'Last leave': '11/14/22, 6:42:47 PM', 'In-meeting duration': '1h 42m 1s', 'Email': 'Ivan.Gonzalez@jala.university', 'Participant ID (UPN)': 'Ivan.Gonzalez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Sebastian Arbelaez Meneses', 'First join': '11/14/22, 5:01:04 PM', 'Last leave': '11/14/22, 6:30:42 PM', 'In-meeting duration': '1h 29m 37s', 'Email': 'Sebastian.Arbelaez@jala.university', 'Participant ID (UPN)': 'Sebastian.Arbelaez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Brayan Vasquez Rojas', 'First join': '11/14/22, 5:02:04 PM', 'Last leave': '11/14/22, 6:42:40 PM', 'In-meeting duration': '1h 40m 36s', 'Email': 'Brayan.Vasquez@jala.university', 'Participant ID (UPN)': 'Brayan.Vasquez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Alex Perez Castillo', 'First join': '11/14/22, 5:02:08 PM', 'Last leave': '11/14/22, 6:42:41 PM', 'In-meeting duration': '1h 40m 32s', 'Email': 'Alex.Perez@jala.university', 'Participant ID (UPN)': 'Alex.Perez@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Vanessa Tejerina Flores', 'First join': '11/14/22, 5:18:39 PM', 'Last leave': '11/14/22, 6:42:38 PM', 'In-meeting duration': '1h 23m 58s', 'Email': 'Vanessa.Tejerina@jala.university', 'Participant ID (UPN)': 'Vanessa.Tejerina@jala.university', 'Role': 'Presenter'}, 
        {'Name': 'Jose Mendoza Antonio', 'First join': '11/14/22, 6:34:47 PM', 'Last leave': '11/14/22, 6:42:45 PM', 'In-meeting duration': '1m 22s', 'Email': 'Jose.Mendoza@jala.university', 'Participant ID (UPN)': 'Jose.Mendoza@jala.university', 'Role': 'Presenter'}]
    
    in_meeting_activities = ParticipantList(in_meeting_activities)
    participants = in_meeting_activities.participants
    assert expected_participants == participants
    
    
def test_in_meeting_activities_is_empty ():
    in_meeting_activities = ParticipantList([])
    participants = in_meeting_activities.participants
    assert [] == participants    


def test_in_meeting_activities_none ():
    in_meeting_activities = ParticipantList(None)
    participants = in_meeting_activities.participants
    assert None is participants  
    