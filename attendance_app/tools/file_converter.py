import sys 
import os


__top_dir_name = os.path.dirname(__file__)
is_win = '\\' in __top_dir_name
separator = '\\' if is_win else '/'
__top_dir_name = __top_dir_name[::-1].split(separator, 1)[-1][::-1]
__default_src_path = f"{__top_dir_name}{separator}src"
__default_data_path = os.path.normcase(f'{__top_dir_name}{separator}data')
__default_stage_path = os.path.normcase(f'{__top_dir_name}{separator}stage_data')
sys.path.extend([__top_dir_name, __default_src_path, __default_stage_path, __default_data_path])


from src.logger_wrapper import setup_logger
from src.teams_files_reader import load_data


logger =  setup_logger(__name__)


def main():
    logger.info("Starting Main Tool")
    # TODO: do my stuff
    load_data()
    logger.info("Finishing Main Tool")


if __name__ == '__main__':
    main()
