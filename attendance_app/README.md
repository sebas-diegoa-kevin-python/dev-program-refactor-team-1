# NEW TOOL ATTENDANCE
Transform all files in V2 format. 

#### By Team 1 (Python Training):
* Miguel Angel Pozo Morales
* Jose Enrique Mendoza Antonio
* Jose Miguel Gemio Quispe


## Instructions

- Download de project
- Unzip the file
- Go to the main Folder
- You should Modify the path(DEFAULT_PATH) of the folder Tools with your own path in the file *loogger_wrapper.py*.


## Install Requirement

Install dependencies
```python
cd PATH_TO_PROJECT_FOLDER
$pip install -r requirements.txt
```

## Run New Tool
Run the following command:
```python
$python ./tools/file_converter.py
```

## Run Tests
Run the following command:
```python
cd PATH_TO_PROJECT_TESTS_FOLDER
$pytest ./tests/test_tools.py 
```
