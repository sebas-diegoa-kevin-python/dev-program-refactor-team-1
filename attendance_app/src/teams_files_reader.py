import csv
from os import listdir, path, makedirs
from os.path import isfile, join
from src.report_section_helper import *
from src.summary_builder import normalize_summary, build_summary_object
from src.participant_builder import normalize_in_meetings, normalize_participants
from src.logger_wrapper import setup_logger, setup_logger_success
from datetime import datetime
from src.summary import Summary


logger =  setup_logger(__name__)
DIR_LOG = 'attendance_app/log'
STAGE_DATA = 'attendance_app/stage_data'
FORMAT_DIR_NAME = '%Y%m%d%H%M%S'
MEETING_SUMMARY = 'Meeting Summary'
FULL_NAME =  'Full Name'
PARTICIPANTS  = '2. Participants'
ACTIVITIES = '3. In-Meeting activities'


def file_meet_conditions(path, pattern, file_path):
    return isfile(join(path, file_path)) and file_path.endswith(pattern)


# discovery de "data" files
def discover_data_files(path='attendance_app/data', pattern='csv'):
    return [join(path, f) for f in listdir(path) if file_meet_conditions(path, pattern, f)]


def get_file_data(path):
    with open(path, encoding='UTF-16') as reader:
        return list(csv.reader(reader, delimiter='\t'))


def get_summary_as_dict(summary):
    # convert list that contains "summary" lines into dict 
    summary_dict = {}
    for element in summary:
        _, row = element
        # TODO must guarante index 0 and 1 exist
        summary_dict[row[0]] = row[1]
    return summary_dict


def get_data_as_dict(data):
    if data == []:
        return data

    result = []
    _, header = data[0] # TODO must guarante index 0 exists
    for _, row in data:
        result.append(dict(zip(header,row)))
    return result


def get_data_as_dict_v2(data: list, section='In-meeting-activities'):
    if not data:
        return []
    result = []
    _, header = data[0] 
    data.pop(0)
    for _, row in data:
        result.append(dict(zip(header,row)))
    
    if len(data) == 0:
        logger.warning(f'The section of {section} of the file is empty...')
        
    return result


def normalize_raw_data(data):
    is_old_file_version = [MEETING_SUMMARY] in data
    # extract data sections
    summary =  extract_section_rows(data, MEETING_SUMMARY, FULL_NAME) if is_old_file_version else extract_section_rows(data, '1. Summary', PARTICIPANTS)
    participants  = [] if is_old_file_version else extract_section_rows(data, PARTICIPANTS, ACTIVITIES)
    in_meetings = extract_section_rows(data, FULL_NAME, include_start=True) if is_old_file_version else extract_section_rows(data, ACTIVITIES)
    # convert list that contains "summary" lines into dict 
    summary = get_summary_as_dict(summary)
    # participants = get_data_as_dict_v2(participants)
    in_meetings = get_data_as_dict_v2(in_meetings)

    return summary, in_meetings


def proccess_data(data: list, fp: str, output_path: str, success_path: str) -> None:
    summary, in_meetings = normalize_raw_data(data)
    summary = build_summary_object(summary)
    participants = normalize_participants(in_meetings)
    in_meetings = normalize_in_meetings(in_meetings)
    write_summary(output_path, summary)
    write_participants(output_path, participants)
    write_in_meetings(output_path, in_meetings)
    with open(success_path, 'a') as success_file:
        success_file.write(f'{summary.attended_participants}, {path.basename(fp)}\n')


def load_data():
    file_paths = discover_data_files() or []    
    PARENT_DIRECTORY = create_folder_in_stage_data()
    LOG_PATH = create_dir(path.basename(PARENT_DIRECTORY), DIR_LOG)
    SUCCESS_PATH = LOG_PATH + '/success.txt'
    ERROR_PATH = LOG_PATH + '/error.txt'
    sw_error, sw_success  = False, False
    
    for fp in file_paths:
        try:
            data = get_file_data(fp)
            logger.info(f'Proccessing the file: {fp}')
            output_path = f'{PARENT_DIRECTORY}/{path.basename(fp)}' 
            proccess_data(data, fp, output_path, SUCCESS_PATH)
            logger.info(f'Proccess data Succsessfully of file: {fp}')
            sw_success = True
        except Exception as e:
            logger.error(f'Failed proccesing data of: {fp}, Error: {e}')
            with open(ERROR_PATH, 'a') as success_file:
                success_file.write(f'{path.basename(fp)}, {e}\n')
            sw_error = True
        
    if not sw_success:
        with open(SUCCESS_PATH, 'w+') as success_file:
            success_file.write('0')
    if not sw_error:
        with open(ERROR_PATH, 'w+') as error_file:
            error_file.write('0')

            
def write_row_data(output_path, row):
    try:
        with open(output_path, 'a', encoding='UTF-8', newline="") as file:
            writer = csv.writer(file, delimiter='\t')
            writer.writerow(row)
    except FileNotFoundError:
        raise FileNotFoundError
    return {'statusCode': 'ok', 'body': None}


def create_folder_in_stage_data():
    dir_name = datetime.now().strftime(FORMAT_DIR_NAME)
    return create_dir(dir_name)


def create_dir(dir_name, path = STAGE_DATA):
    if dir_name is None or path is None:
        return "Folder cannot be None"
    if dir_name =="" or path =="":
        return "Folder cannot be Empty"
    try:
        dir_folder = f'{path}/{dir_name}'
        # mkdir(dir_folder)
        makedirs(dir_folder, exist_ok=True)
        return dir_folder
    except OSError as e:
        return f'Error in creation /{dir_name}, {e} '


def write_summary(output_path, summary):
    if type(summary) is not Summary:
        raise TypeError('write_summary expect Objects Summary')
    write_row_data(output_path, ['1. Summary'])
    write_row_data(output_path, ['Meeting title', summary.title])
    write_row_data(output_path, ['Attended participants', summary.attended_participants])
    write_row_data(output_path, ['Start time', summary.start_time])
    write_row_data(output_path, ['End time', summary.end_time])
    write_row_data(output_path, ['Meeting duration', summary.duration])
    write_row_data(output_path, ['Meeting id', summary.id])


def write_in_meetings(output_path: str, data_list: list[dict]):
    HEADERS = ['Name','Join time','Leave time','Duration','Email','Role']
    write_row_data(output_path, [])
    write_row_data(output_path, ['3. In-Meeting activities'])
    write_row_data(output_path, HEADERS)
    for data_dict in data_list:
        write_row_data(output_path, list(data_dict.values()))


def write_participants(output_path, participant_list):
    HEADERS = ['Name', 'First join', 'Last leave', 'In-meeting duration', 'Email', 'Participant ID (UPN)', 'Role']
    write_row_data(output_path, [])
    write_row_data(output_path, [PARTICIPANTS])
    write_row_data(output_path, HEADERS)
    for participant in participant_list:
        write_row_data(output_path, list(participant.values()))